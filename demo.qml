import QtQuick 2.0
import QtQuick.Controls 1.1
import "components" as Components

ApplicationWindow {
    title: "Docker"
    id: root
    width: 600
    height: 400
    minimumHeight: 400
    minimumWidth: 570

    TabView {
        id:frame
        //enabled: enabledCheck.checked
        //tabPosition: controlPage.item ? controlPage.item.tabPosition : Qt.TopEdge
        anchors.fill: parent
        anchors.margins: Qt.platform.os === "osx" ? 12 : 2

        Components.ContainersTab {}
        Components.ImagesTab {}
    }
}
