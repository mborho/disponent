package main

import (
	"fmt"
	"github.com/fsouza/go-dockerclient"
	"github.com/niemeyer/qml"
	"os"
	"reflect"
	"strings"
)

type Service struct {
	baseUrl string
}

func (s *Service) SetLabel(parent qml.Object, new_label string) {
	parent.Set("text", new_label)
}

func main() {
	if err := run(); err != nil {
		fmt.Fprintf(os.Stderr, "error: %v\n", err)
		os.Exit(1)
	}
	os.Exit(0)
}

func run() error {
	qml.Init(nil)
	engine := qml.NewEngine()

	component, err := engine.LoadFile("demo.qml")
	if err != nil {
		return err
	}

	service := Service{
		baseUrl: "http://ubuntu.com",
	}

	context := engine.Context()
	context.SetVar("test", &service)

	// docker client
	endpoint := "unix:///var/run/docker.sock"
	client, _ := docker.NewClient(endpoint)
	// images
	images := &Images{
		client: *client,
	}
	engine.Context().SetVar("images", images)
	// containers
	containers := &Containers{
		client: *client,
	}
	engine.Context().SetVar("containers", containers)

	win := component.CreateWindow(nil)

	/*blockComponent, err := engine.LoadFile("Block.qml")
	if err != nil {
		return err
	}

	block := &Block{Component: blockComponent}
	game.Block = block

	game.Score = win.Root().ObjectByName("score")*/

	win.Show()
	win.Wait()

	return nil
}

type Images struct {
	list   []docker.APIImages
	Len    int
	client docker.Client
}

func (images *Images) List() {
	images.Len = 0
	images.list = nil
	imgs, _ := images.client.ListImages(true)
	for _, img := range imgs {
		images.list = append(images.list, img)
		images.Len = len(images.list)
		fmt.Println("ID: ", img.ID)
		fmt.Println("RepoTags: ", img.RepoTags)
		fmt.Println("Created: ", img.Created)
		fmt.Println("Size: ", img.Size)
		fmt.Println("VirtualSize: ", img.VirtualSize)
		fmt.Println("ParentId: ", img.ParentId)
		fmt.Println("Repository: ", img.Repository)
	}
	qml.Changed(images, &images.Len)
}

func (images *Images) Value(index int, prop_name string) string {
	return images.list[index].ID
}

type Containers struct {
	list   []docker.APIContainers
	Len    int
	client docker.Client
}

func (containers *Containers) List(all bool) {
	fmt.Println("Checked: ", all)
	containers.Len = 0
	containers.list = nil
	options := docker.ListContainersOptions{}
	if all {
		options.All = true
		options.Limit = 50
	}
	conts, _ := containers.client.ListContainers(options)
	for _, cont := range conts {
		containers.list = append(containers.list, cont)
		containers.Len = len(containers.list)
		fmt.Println("ID: ", cont.ID)
		//fmt.Println(strings.Join(cont.Names, ", "))
	}
	qml.Changed(containers, &containers.Len)
}

func (containers *Containers) Value(index int, propName string) string {
	var result string
	var container docker.APIContainers
	container = containers.list[index]
	immutable := reflect.ValueOf(container)
	if propName == "Names" {
		result = immutable.FieldByName("Names").Index(0).String()
	} else {
		result = immutable.FieldByName(propName).String()
	}
	return strings.TrimSpace(result)
}

/*func (images *Images) Add(img docker.APIImages) {
	images.list = append(images.list, img)
	images.Len = len(images.list)
	qml.Changed(images, &images.Len)
}*/
